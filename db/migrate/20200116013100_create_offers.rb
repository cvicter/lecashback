class CreateOffers < ActiveRecord::Migration[5.2]
  def change
    create_table :offers do |t|
      t.references :advertiser, foreign_key: true
      t.string :url
      t.text :description
      t.datetime :start_at
      t.datetime :end_at
      t.boolean :premium
      t.boolean :state

      t.timestamps
    end
  end
end
