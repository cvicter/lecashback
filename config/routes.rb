Rails.application.routes.draw do
  resources :offers
  resources :advertisers
  resources :administrators
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'home#index'
  get '/offers/change_state/:id', to: 'offers#change_state', as: 'change_offer_state'

  require 'sidekiq/web'

  mount Sidekiq::Web => '/sidekiq'

end
