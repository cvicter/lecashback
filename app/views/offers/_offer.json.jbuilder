json.extract! offer, :id, :partner_id, :advertiser_id, :url, :description, :start_at, :end_at, :premium, :state, :created_at, :updated_at
json.url offer_url(offer, format: :json)
