class HomeController < ApplicationController
  def index
    @offer = Offer.joins(:advertiser)
            .order(premium: :desc)
            .where(state: true)

  end
end
