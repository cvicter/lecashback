class OffersController < ApplicationController
  before_action :set_offer, only: [:show, :edit, :update, :destroy]

  # GET /offers
  # GET /offers.json
  def change_state
    @offer = Offer.find(params[:id])
    #@offer = Offer.find(offer_id)
    new_state = set_state(@offer)

    if @offer.state != new_state
      @offer.state = new_state
      if @offer.save
        state = @offer.state ? 'enabled' : 'disabled'
        redirect_to offers_url, notice: "Offer was successfully #{state}"
        # SetEnableStateWorker.perform_at(@offer.start_at, @offer.id)
        # SetDisableStateWorker.perform_at(@offer.end_at, @offer.id)
      else
        redirect_to offers_url, notice: 'Offer was not updated.'
      end
    else
      redirect_to offers_url
    end

  end

  def index
    @offers = Offer.all
  end

  # GET /offers/1
  # GET /offers/1.json
  def show
  end

  # GET /offers/new
  def new
    @offer = Offer.new
  end

  # GET /offers/1/edit
  def edit
  end

  # POST /offers
  # POST /offers.json
  def create


    @offer = Offer.new(offer_params)
    @offer.state = false

    respond_to do |format|
      if @offer.save
        format.html { redirect_to offers_url, notice: 'Offer was successfully created.' }
        format.json { render :show, status: :created, location: @offer }

        # SetEnableStateWorker.perform_at(@offer.start_at, @offer.id)
        # SetDisableStateWorker.perform_at(@offer.end_at, @offer.id)

      else
        format.html { render :new }
        format.json { render json: @offer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /offers/1
  # PATCH/PUT /offers/1.json
  def update
    respond_to do |format|
      if @offer.update(offer_params)
        format.html { redirect_to offers_url, notice: 'Offer was successfully updated.' }
        format.json { render :show, status: :ok, location: @offer }
      else
        format.html { render :edit }
        format.json { render json: @offer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /offers/1
  # DELETE /offers/1.json
  def destroy
    @offer.destroy
    respond_to do |format|
      format.html { redirect_to offers_url, notice: 'Offer was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_offer
      @offer = Offer.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def offer_params
      params.require(:offer).permit(:advertiser_id, :url, :description, :start_at, :end_at, :premium, :state)
    end

    def set_state(offer)

      if !offer.state

        if Time.now >= offer.start_at && offer.end_at.blank?
          return true
        end

        if Time.now >= offer.start_at && Time.now < offer.end_at
          return true
        end

      else

        if !offer.end_at.blank?
          if Time.now <= offer.end_at
            return false
          end
        end

        return true

      end

    end

end
