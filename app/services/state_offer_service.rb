class StateOfferService
  def self.set_enable!(offer_id)
    offer = Offer.find(offer_id)
    offer.state = true
    offer.save!
    puts 'set_enable!!!!!'
  end

  def self.set_disable!(offer_id)
    offer = Offer.find(offer_id)
    offer.state = false
    offer.save!
    puts 'set_disable!!!!!'
  end
end
