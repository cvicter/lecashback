class SetEnableStateWorker
  include Sidekiq::Worker

  def perform(offer_id)
    StateOfferService.set_enable!(offer_id)
  end

end
