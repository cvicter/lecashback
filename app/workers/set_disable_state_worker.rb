class SetDisableStateWorker
  include Sidekiq::Worker

  def perform(offer_id)
    StateOfferService.set_disable!(offer_id)
  end

end
