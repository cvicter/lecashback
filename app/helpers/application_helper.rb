module ApplicationHelper
  def active_item(name)
    params[:controller] == name ? "active" : ""
  end
end
