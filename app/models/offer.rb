class Offer < ApplicationRecord
  belongs_to :advertiser
  validates :url, :description, :start_at, presence: true
  # validates :url, format: URI::regexp(%w[http https]),
  validates :url, presence: true, uniqueness: true
  validates :description, length: { minimum: 5, maximum: 500 }
end
