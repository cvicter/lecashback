class Administrator < ApplicationRecord
  validates :name, presence: true, uniqueness: true
end
